# Hadoop-temperatureAverage

#### BIG DATA Postgraduation Course <br> Universidade Positivo - Curitiba - Brazil <br> By André Silva Alves @andresilvaalves

## commands to execute Java program

###### --Create directories in the HDFS

```
$ hadoop fs -mkdir /user/cloudera/andresilvaalves 
$ hadoop fs -mkdir /user/cloudera/andresilvaalves/input
```

###### --copy files to directory
```
$ hadoop fs -put 1901 1902 /user/cloudera/andresilvaalves/input
```
###### -- command to execute java program

```
$ hadoop jar hadoop-temperatureAverage.jar /user/cloudera/andresilvaalves/input /user/cloudera/andresilvaalves/output
```
###### -- reading results and copying to one text file

```
$ hadoop fs -cat /user/cloudera/andresilvaalves/output/* > result
$ cat result
```

###### --deleting directory

```
$ hadoop fs -rm -r /user/cloudera/andresilvaalves/output
```
## Alternative

#### Commands to execute PIG script

https://gist.github.com/andresilvaalves/49e816050fd310d7da0f9d4a8e51c172

###### --Create drirectoties in the HDFS (if necessary)

```
$ hadoop fs -mkdir /user/cloudera/andresilvaalves 
$ hadoop fs -mkdir /user/cloudera/andresilvaalves/input
```
###### --Copy files to directory(If necessary)

```
$ hadoop fs -put 1901 1902 /user/cloudera/andresilvaalves/input
```

###### -- execute the Pig script
```
$ pig averageTemperature.pig
```
###### --reading results (only until the group by) 
```
$ hadoop fs -cat /user/cloudera/andresilvaalves/output_pig/* > result_pig
$ cat result_pig
```
###### --deleting directory
```
$ hadoop fs -rm -r /user/cloudera/andresilvaalves/output_pig
```
