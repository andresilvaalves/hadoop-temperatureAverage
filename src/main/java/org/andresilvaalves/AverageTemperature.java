package org.andresilvaalves;

import org.andresilvaalves.map.AverageMap;
import org.andresilvaalves.reduce.AverageReduce;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

public class AverageTemperature extends Configured implements Tool {

	private static final Logger LOG = Logger.getLogger(AverageTemperature.class);

	private static /*final*/ String PATH_INPUT  = "" /*"/user/cloudera/andresilvaalves/input"*/;
	private static /*final*/ String PATH_OUTPUT = "" /*"/user/cloudera/andresilvaalves/output"*/;

	public static void main(String[] args) throws Exception {
		LOG.info("Starting AverageTemperature");

		int res = ToolRunner.run(new AverageTemperature(), args);

		LOG.info("End AverageTemperature");

		System.exit(res);
	}

	public int run(String[] args) throws Exception {

		if (args.length != 2) {
			LOG.error("Please Enter the input and output parameters");
			System.exit(-1);
		}
		PATH_INPUT  = args[0];
		PATH_OUTPUT = args[1];
		
		LOG.info("PATH_INPUT = "+PATH_INPUT);
		LOG.info("PATH_OUTPUT = "+PATH_OUTPUT);
		
		readFiles();

		Job job = Job.getInstance(getConf(), "AverageTemperature");
		job.setJarByClass(this.getClass());

		FileInputFormat.addInputPath(job, new Path(PATH_INPUT));
		FileOutputFormat.setOutputPath(job, new Path(PATH_OUTPUT));

		job.setMapperClass(AverageMap.class);
		job.setReducerClass(AverageReduce.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	private void readFiles() throws Exception {
		LOG.info("Listing files on HDFS....");

		FileSystem fileSystem = FileSystem.get(getConf());

		RemoteIterator<LocatedFileStatus> listFiles = fileSystem.listFiles(new Path(PATH_INPUT), false);

		while (listFiles.hasNext()) {
			LocatedFileStatus fileStatus = listFiles.next();
			LOG.info("File Name = " + fileStatus.getPath());

		}
	}

}
