package org.andresilvaalves.map;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

public class AverageMap extends Mapper<LongWritable, Text, Text, DoubleWritable> {

	private static final Logger LOG = Logger.getLogger(AverageMap.class);
	
	
	@Override
	protected void map(LongWritable key, Text lineText, Mapper<LongWritable, Text, Text, DoubleWritable>.Context context)
			throws IOException, InterruptedException {

		String line = lineText.toString();
		
		try {
			
			if(StringUtils.isNotBlank(line)){
				
				String year = StringUtils.substring(line, 15,19);
				String temperatureStr = "";
				String charAt = StringUtils.substring(line, 87,1);
				
				if ( StringUtils.equals(charAt, "+") ){
					temperatureStr = line.substring(88, 92);
				}else{
					temperatureStr = line.substring(87, 92);       
				}				
				Double temperature = new Double(temperatureStr);
				
				LOG.info("Year = "+year);
				LOG.info("Temperature = "+temperature);
				
				context.write(new Text(year), new DoubleWritable(temperature));
			}
			
		} catch (IOException | InterruptedException e) {
			LOG.error("Error=",e);
			 throw e;
		}

	}
}
